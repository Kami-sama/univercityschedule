package com.kknau.service;

import com.kknau.dto.SchedulerDTO;
import com.kknau.model.Scheduler;
import com.kknau.model.enums.FractionType;
import com.kknau.model.enums.WeekDay;
import com.kknau.repository.AuditoryRepository;
import com.kknau.repository.FlockRepository;
import com.kknau.repository.SubjectRepository;
import com.kknau.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConvertService {

    @Autowired
    private FlockRepository flockRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private AuditoryRepository auditoryRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    //TODO: create validate for scheduler
    public Scheduler convert(SchedulerDTO dto) {
        Scheduler scheduler = new Scheduler();

        scheduler.setFlock(flockRepository.findOne(Integer.valueOf(dto.getFlock())));
        scheduler.setTeacher(teacherRepository.findOne(Integer.valueOf(dto.getTeacher())));
        scheduler.setAuditory(auditoryRepository.findOne(Integer.valueOf(dto.getAuditory())));
        scheduler.setSubject(subjectRepository.findOne(Integer.valueOf(dto.getSubject())));
        scheduler.setLessonNumber(Byte.valueOf(dto.getLessonNumber()));
        scheduler.setFraction(FractionType.valueOf(dto.getFraction()));
        scheduler.setWeekDay(WeekDay.valueOf(dto.getWeekDay()));

        return scheduler;

    }
}
