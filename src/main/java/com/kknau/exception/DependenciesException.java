package com.kknau.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY, reason = "Возможно на данную сущность, есть зависимости!")
public class DependenciesException extends RuntimeException {
}
