package com.kknau.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Не найдено такой страницы!")
public class NotFoundException extends RuntimeException {
}
