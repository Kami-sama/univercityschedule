package com.kknau.model.enums;

public enum LessonType {
    LABORATORY {
        @Override
        public String toString() {
            return "Лабораторная";
        }
    }, LECTION {
        @Override
        public String toString() {
            return "Лекция";
        }
    }, PRACTICE {
        @Override
        public String toString() {
            return "Практическая";
        }
    }
}
