package com.kknau.model.enums;

public enum WeekDay {
    MONDAY("Понеділок"), TUESDAY("Вівторок"), WEDNESDAY("Середа"), THURSDAY("Четверг"), FRIDAY("П'ятниця"), SATURDAY("Субота");
    private String name;

    WeekDay(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
