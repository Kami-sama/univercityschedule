package com.kknau.model.enums;

public enum FractionType {
    NUMERATOR{
        @Override
        public String toString() {
            return "числитель";
        }
    }, DENOMINATOR{
        @Override
        public String toString() {
            return "знаменатель";
        }
    }
}
