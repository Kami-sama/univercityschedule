package com.kknau.model;

import com.kknau.model.enums.LessonType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Auditory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String number;

    private Integer amountWorkPlace;

    @Enumerated(EnumType.ORDINAL)
    private LessonType auditoryType;

    public Auditory(String number, Integer amountWorkPlace, LessonType auditoryType) {
        this.number = number;
        this.amountWorkPlace = amountWorkPlace;
        this.auditoryType = auditoryType;
    }

    public Auditory() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmountWorkPlace() {
        return amountWorkPlace;
    }

    public void setAmountWorkPlace(Integer amountWorkPlace) {
        this.amountWorkPlace = amountWorkPlace;
    }

    public LessonType getAuditoryType() {
        return auditoryType;
    }

    public void setAuditoryType(LessonType auditoryType) {
        this.auditoryType = auditoryType;
    }
}
