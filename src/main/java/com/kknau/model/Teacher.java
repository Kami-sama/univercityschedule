package com.kknau.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Teacher implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String name;

    private String position;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Subject> subjects;

    public Teacher() {
    }

    public Teacher(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void addSubject(Subject subject) {
        if (subjects == null) {
            subjects = new ArrayList<>();
        }
        subjects.add(subject);
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void deleteSubject(Subject subject) {
        if (subjects != null) {
            subjects.remove(subject);
        }
    }

    public boolean consistSubject(Subject subject) {
        return subjects.contains(subject);
    }

    public String getCutName() {
        String[] s = name.split("\\s");
        int l = s.length;
        if (l == 0) {
            return null;
        } else if (l <= 1) {
            return s[0];
        }

        StringBuilder result = new StringBuilder(s[0]);
        result.append(" ");
        result.append(s[1].charAt(0));
        result.append(". ");
        result.append(s[2].charAt(0));
        result.append(". ");

        return result.toString();
    }
}
