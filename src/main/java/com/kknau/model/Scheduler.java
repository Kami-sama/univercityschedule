package com.kknau.model;

import com.kknau.model.enums.FractionType;
import com.kknau.model.enums.LessonType;
import com.kknau.model.enums.WeekDay;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Scheduler implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "flock_id")
    private Flock flock;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "auditory_id")
    private Auditory auditory;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    private Byte lessonNumber;

    @Enumerated(EnumType.ORDINAL)
    private FractionType fraction;

    @Enumerated(EnumType.ORDINAL)
    private WeekDay weekDay;


    public Scheduler() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Flock getFlock() {
        return flock;
    }

    public void setFlock(Flock flock) {
        this.flock = flock;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Auditory getAuditory() {
        return auditory;
    }

    public void setAuditory(Auditory auditory) {
        this.auditory = auditory;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Byte getLessonNumber() {
        return lessonNumber;
    }

    public void setLessonNumber(Byte lessonNumber) {
        this.lessonNumber = lessonNumber;
    }

    public FractionType getFraction() {
        return fraction;
    }

    public void setFraction(FractionType fraction) {
        this.fraction = fraction;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    public Scheduler(Flock flock, Teacher teacher, Auditory auditory, Subject subject, LessonType lessonType, Byte lessonNumber, FractionType fraction) {
        this.flock = flock;
        this.teacher = teacher;
        this.auditory = auditory;
        this.subject = subject;
        this.lessonNumber = lessonNumber;
        this.fraction = fraction;
    }

    @Override
    public String toString() {
        return "Scheduler{" +
                "id=" + id +
                ", flock=" + flock +
                ", teacher=" + teacher +
                ", auditory=" + auditory +
                ", subject=" + subject +
                ", lessonNumber=" + lessonNumber +
                ", fraction=" + fraction +
                ", weekDay=" + weekDay +
                '}';
    }
}

