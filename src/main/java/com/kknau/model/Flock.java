package com.kknau.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Flock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Temporal(value = TemporalType.DATE)
    private Date startDate;
    @Temporal(value = TemporalType.DATE)
    private Date endDate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "faculty_id")
    private Faculty faculty;

//    private Byte size;

    @Transient
    private Integer course;

    public Flock(String name, Date startDate, Date endDate, Faculty faculty) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.faculty = faculty;
    }

    public Flock() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

//    public Byte getSize() {
//        return size;
//    }
//
//    public void setSize(Byte amountPeople) {
//        this.size = amountPeople;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flock flock = (Flock) o;

        if (endDate != null ? !endDate.equals(flock.endDate) : flock.endDate != null) return false;
        if (faculty != null ? !faculty.equals(flock.faculty) : flock.faculty != null) return false;
        if (name != null ? !name.equals(flock.name) : flock.name != null) return false;
        if (startDate != null ? !startDate.equals(flock.startDate) : flock.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (faculty != null ? faculty.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Flock{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", faculty=" + faculty +
                '}';
    }
}
