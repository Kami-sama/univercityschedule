package com.kknau.repository;

import com.kknau.model.Auditory;
import com.kknau.model.Flock;
import com.kknau.model.Scheduler;
import com.kknau.model.Teacher;
import com.kknau.model.enums.FractionType;
import com.kknau.model.enums.WeekDay;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SchedulerRepository extends CrudRepository<Scheduler, Integer> {
    public Scheduler findByFlockAndLessonNumberAndFractionAndWeekDay(Flock flock, Byte lessonNumber, FractionType fractionType, WeekDay weekDay);

    public Scheduler findByTeacherAndLessonNumberAndFractionAndWeekDay(Teacher teacher, Byte lessonNumber, FractionType fractionType, WeekDay weekDay);

    public Scheduler findByAuditoryAndLessonNumberAndFractionAndWeekDay(Auditory auditory, Byte lessonNumber, FractionType fractionType, WeekDay weekDay);

    public List<Scheduler> findByFlock_Id(Integer flockId);
}
