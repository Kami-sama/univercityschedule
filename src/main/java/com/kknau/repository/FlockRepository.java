package com.kknau.repository;

import com.kknau.model.Faculty;
import com.kknau.model.Flock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FlockRepository extends CrudRepository<Flock, Integer> {

    public List<Flock> findByFaculty(Faculty faculty);

    @Query("select f from Flock f where faculty.id=?1")
    public List<Flock> findByFacultyId(Integer id);

    public Flock findByName(String name);
}
