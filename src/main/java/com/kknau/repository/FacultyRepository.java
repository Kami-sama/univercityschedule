package com.kknau.repository;

import com.kknau.model.Faculty;
import org.springframework.data.repository.CrudRepository;

public interface FacultyRepository extends CrudRepository<Faculty, Integer> {
    public Faculty findByName(String name);
}
