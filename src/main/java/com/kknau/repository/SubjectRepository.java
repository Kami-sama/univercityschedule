package com.kknau.repository;

import com.kknau.model.Subject;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, Integer> {
    Subject findByName(String name);
}
