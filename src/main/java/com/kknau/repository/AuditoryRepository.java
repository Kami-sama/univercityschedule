package com.kknau.repository;

import com.kknau.model.Auditory;
import com.kknau.model.enums.LessonType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AuditoryRepository extends CrudRepository<Auditory, Integer> {
    public Auditory findByNumber(String number);

    List<Auditory> findByAuditoryType(LessonType auditoryType);
}
