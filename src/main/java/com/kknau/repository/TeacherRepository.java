package com.kknau.repository;

import com.kknau.model.Subject;
import com.kknau.model.Teacher;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeacherRepository extends CrudRepository<Teacher, Integer> {
    public List<Teacher> findBySubjects(Subject subject);

    public Teacher findByName(String name);
}
