package com.kknau.controller;

import com.kknau.model.enums.WeekDay;
import com.kknau.repository.FacultyRepository;
import com.kknau.repository.FlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
@RequestMapping("find")
public class FindController {

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private FlockRepository flockRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView faculty() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("faculties", facultyRepository.findAll());
        map.put("days", WeekDay.values());
        return new ModelAndView("find", map);
    }


}
