package com.kknau.controller;

import com.kknau.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloController {

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private FlockRepository flockRepository;

    @Autowired
    private AuditoryRepository auditoryRepository;

    @Autowired
    private SchedulerRepository schedulerRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        return "hello";
    }

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "tables", method = RequestMethod.GET)
    public ModelAndView tables() {
        return new ModelAndView("tables");
    }

    @RequestMapping(value = "forms", method = RequestMethod.GET)
    public ModelAndView forms() {
        return new ModelAndView("forms");
    }

    @RequestMapping(value = "charts", method = RequestMethod.GET)
    public ModelAndView charts() {
        return new ModelAndView("charts");
    }

    @RequestMapping(value = "bootstrap-grid", method = RequestMethod.GET)
    public ModelAndView bootstrap_grid() {
        return new ModelAndView("bootstrap-grid");
    }

    @RequestMapping(value = "bootstrap-elements", method = RequestMethod.GET)
    public ModelAndView bootstrap_elements() {
        return new ModelAndView("bootstrap-elements");
    }

    @RequestMapping(value = "blank-page", method = RequestMethod.GET)
    public ModelAndView blank_page() {
        return new ModelAndView("blank-page");
    }

}