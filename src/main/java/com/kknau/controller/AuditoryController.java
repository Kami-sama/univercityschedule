package com.kknau.controller;

import com.kknau.dto.AuditoryDTO;
import com.kknau.exception.DependenciesException;
import com.kknau.exception.ExisistException;
import com.kknau.model.Auditory;
import com.kknau.model.enums.LessonType;
import com.kknau.repository.AuditoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
@RequestMapping("auditory")
public class AuditoryController {

    @Autowired
    private AuditoryRepository auditoryRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView auditory() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("lessonTypes", LessonType.values());
        map.put("auditories", auditoryRepository.findAll());
        return new ModelAndView("auditory", map);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Auditory addAuditory(@RequestBody AuditoryDTO dto) {
        if (dto.getNumber().isEmpty()) {
            return null;
        }
        Auditory auditory = auditoryRepository.findByNumber(dto.getNumber());
        if (auditory != null) {
            throw new ExisistException();
        }
        return auditoryRepository.save(new Auditory(dto.getNumber(), Integer.valueOf(dto.getAmountWorkPlace()), LessonType.valueOf(dto.getAuditoryType())));
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteAuditoryPost(@RequestParam Integer id) {
        try {
            auditoryRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
    }
}
