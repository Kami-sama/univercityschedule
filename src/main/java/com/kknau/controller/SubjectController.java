package com.kknau.controller;

import com.kknau.exception.DependenciesException;
import com.kknau.exception.ExisistException;
import com.kknau.model.Subject;
import com.kknau.repository.SubjectRepository;
import com.kknau.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
@RequestMapping("subject")
public class SubjectController {

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView subject() {
        return new ModelAndView("subject", "subjects", subjectRepository.findAll());
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Subject addSubject(@RequestBody String name) {
        if (name.isEmpty()) {
            return null;
        }
        Subject subject = subjectRepository.findByName(name);
        if (subject != null) {
            throw new ExisistException();
        }
        return subjectRepository.save(new Subject(name));
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteSubject(@RequestParam Integer id) {
        try {
            subjectRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
        return subject();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteSubjectPost(@RequestParam Integer id) {
        try {
            subjectRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    private ModelAndView getSubject(@RequestParam Integer id) {
        HashMap<String, Object> map = new HashMap<>();
        Subject subject = subjectRepository.findOne(id);
        map.put("subject", subject);
        map.put("teachers", teacherRepository.findBySubjects(subject));
        return new ModelAndView("subjectPage", map);
    }

}
