package com.kknau.controller;

import com.kknau.dto.FacultyDTO;
import com.kknau.exception.DependenciesException;
import com.kknau.exception.ExisistException;
import com.kknau.exception.NotFoundException;
import com.kknau.model.Faculty;
import com.kknau.model.Flock;
import com.kknau.repository.FacultyRepository;
import com.kknau.repository.FlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("faculty")
public class FacultyController {

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private FlockRepository flockRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView faculty() {
        return new ModelAndView("faculty", "faculties", facultyRepository.findAll());
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Faculty addFaculty(@RequestBody FacultyDTO dto) {
        if (dto.getName().isEmpty()) {
            return null;
        }
        Faculty faculty = facultyRepository.findByName(dto.getName());
        if (faculty != null) {
            throw new ExisistException();
        }
        return facultyRepository.save(new Faculty(dto.getName(), dto.getAbbreviate(), dto.getPhone()));
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteFaculty(@RequestParam Integer id) {
        try {
            facultyRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
        return faculty();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteFacultyPost(@RequestParam Integer id) {
        try {
            facultyRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ModelAndView getFaculty(@RequestParam Integer id) {
        Faculty faculty = facultyRepository.findOne(id);
        if (faculty == null) {
            throw new NotFoundException();
        }
        List<Flock> flocks = flockRepository.findByFaculty(faculty);
        HashMap<String, Object> map = new HashMap<>();
        map.put("faculty", faculty);
        map.put("flocks", flocks);
        return new ModelAndView("facultyPage", map);
    }
}
