package com.kknau.controller;

import com.kknau.dto.TeacherDTO;
import com.kknau.exception.DependenciesException;
import com.kknau.exception.ExisistException;
import com.kknau.exception.NotFoundException;
import com.kknau.model.Subject;
import com.kknau.model.Teacher;
import com.kknau.repository.SubjectRepository;
import com.kknau.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("teacher")
public class TeacherController {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView teacher() {
        return new ModelAndView("teacher", "teachers", teacherRepository.findAll());
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Teacher addTeacher(@RequestBody TeacherDTO dto) {
        if (dto.getName().isEmpty()) {
            return null;
        }
        Teacher teacher = teacherRepository.findByName(dto.getName());
        if (teacher != null) {
            throw new ExisistException();
        }
        return teacherRepository.save(new Teacher(dto.getName(), dto.getPosition()));
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteTeacher(@RequestParam Integer id) {
        try {
            teacherRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
        return teacher();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteTeacherPost(@RequestParam Integer id) {
        try {
            teacherRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
        return "OK";
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ModelAndView getTeacher(@RequestParam Integer id) {
        Teacher teacher = teacherRepository.findOne(id);
        if (teacher == null) {
            throw new NotFoundException();
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("teacher", teacher);
        map.put("subjects", subjectRepository.findAll());
        return new ModelAndView("teacherPage", map);
    }

    @RequestMapping(value = "addSubject", method = RequestMethod.POST)
    @ResponseBody
    public Subject addSubjectForTeacher(@RequestBody String param) {
        String[] params = param.split(",");
        Subject subject = subjectRepository.findOne(Integer.valueOf(params[0]));
        Teacher teacher = teacherRepository.findOne(Integer.valueOf(params[1]));
        if (teacher.consistSubject(subject)) {
            throw new ExisistException();
        }
        teacher.addSubject(subject);
        teacherRepository.save(teacher);
        return subject;
    }

    @RequestMapping(value = "subjectTeacherDelete", method = RequestMethod.POST)
    @ResponseBody
    public Subject deleteSubjectTeacher(@RequestParam("subject_id") Integer subjectId, @RequestParam("teacher_id") Integer teacherId) {
        Subject subject = subjectRepository.findOne(subjectId);
        Teacher teacher = teacherRepository.findOne(teacherId);
        teacher.deleteSubject(subject);
        teacherRepository.save(teacher);
        return subject;
    }

    @RequestMapping(value = "getSubjectByTeacher", method = RequestMethod.POST)
    @ResponseBody
    public List<Subject> getSubjectByTeacher(@RequestBody Integer id) {
        return teacherRepository.findOne(id).getSubjects();
    }
}
