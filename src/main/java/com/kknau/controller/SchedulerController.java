package com.kknau.controller;

import com.kknau.dto.SchedulerDTO;
import com.kknau.dto.SchedulerMessageDTO;
import com.kknau.exception.DependenciesException;
import com.kknau.model.Auditory;
import com.kknau.model.Flock;
import com.kknau.model.Scheduler;
import com.kknau.model.Teacher;
import com.kknau.model.enums.FractionType;
import com.kknau.model.enums.LessonType;
import com.kknau.model.enums.WeekDay;
import com.kknau.repository.*;
import com.kknau.service.ConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("scheduler")
public class SchedulerController {

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private FlockRepository flockRepository;

    @Autowired
    private AuditoryRepository auditoryRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ConvertService converter;

    @Autowired
    private SchedulerRepository schedulerRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView scheduler() {
        Map<String, Object> map = new HashMap<>();
        map.put("lessonTypes", LessonType.values());
        map.put("fractionTypes", FractionType.values());
        map.put("weekDay", WeekDay.values());
        map.put("faculties", facultyRepository.findAll());
        map.put("auditors", auditoryRepository.findAll());
        map.put("subjects", subjectRepository.findAll());
        map.put("schedulers", schedulerRepository.findAll());
        return new ModelAndView("scheduler", map);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public SchedulerMessageDTO addFaculty(@RequestBody SchedulerDTO dto) {
        Scheduler scheduler = converter.convert(dto);
        Scheduler schedulerByFlock = schedulerRepository.findByFlockAndLessonNumberAndFractionAndWeekDay(scheduler.getFlock(), scheduler.getLessonNumber(), scheduler.getFraction(), scheduler.getWeekDay());
        if (schedulerByFlock != null)
            return new SchedulerMessageDTO(schedulerByFlock.getFlock().getName(), "1");
        Scheduler schedulerByTeacher = schedulerRepository.findByTeacherAndLessonNumberAndFractionAndWeekDay(scheduler.getTeacher(), scheduler.getLessonNumber(), scheduler.getFraction(), scheduler.getWeekDay());
        if (schedulerByTeacher != null)
            return new SchedulerMessageDTO(schedulerByTeacher.getTeacher().getCutName(), "1");
        Scheduler schedulerByAuditory = schedulerRepository.findByAuditoryAndLessonNumberAndFractionAndWeekDay(scheduler.getAuditory(), scheduler.getLessonNumber(), scheduler.getFraction(), scheduler.getWeekDay());
        if (schedulerByAuditory != null) {
            return new SchedulerMessageDTO(schedulerByAuditory.getAuditory().getNumber(), "1");
        }
        schedulerRepository.save(scheduler);
        return new SchedulerMessageDTO("OK", "0");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteScheduler(@RequestParam Integer id) {
        try {
            schedulerRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
        return scheduler();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteSchedulerPost(@RequestParam Integer id) {
        try {
            schedulerRepository.delete(id);
        } catch (Exception e) {
            throw new DependenciesException();
        }
    }

    @RequestMapping(value = "getSchedulersByFlock", method = RequestMethod.POST)
    @ResponseBody
    public List<Scheduler> getSchedulersByFlock(@RequestParam Integer id) {
        List<Scheduler> schedulers1 = schedulerRepository.findByFlock_Id(id);
        Collections.sort(schedulers1, (o1, o2) -> o1.getFraction().compareTo(o2.getFraction()));
        return schedulers1;
    }

    @RequestMapping(value = "getFlocksByFaculty", method = RequestMethod.POST)
    @ResponseBody
    private List<Flock> getFlocksByFaculty(@RequestParam Integer id) {
        return flockRepository.findByFacultyId(id);
    }

    @RequestMapping(value = "getTeachersBySubject", method = RequestMethod.POST)
    @ResponseBody
    private List<Teacher> getTeachersBySubject(@RequestParam Integer id) {
        return teacherRepository.findBySubjects(subjectRepository.findOne(id));
    }

    @RequestMapping(value = "getAuditoriumsByLessonType", method = RequestMethod.POST)
    @ResponseBody
    private List<Auditory> getAuditoriumsByLessonType(@RequestParam String type) {
        return auditoryRepository.findByAuditoryType(LessonType.valueOf(type));
    }

}
