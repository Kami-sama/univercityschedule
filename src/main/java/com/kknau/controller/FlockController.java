package com.kknau.controller;

import com.kknau.dto.FlockDTO;
import com.kknau.exception.ExisistException;
import com.kknau.exception.NotFoundException;
import com.kknau.model.Flock;
import com.kknau.repository.FacultyRepository;
import com.kknau.repository.FlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
@RequestMapping("flock")
public class FlockController {

    @Autowired
    private FlockRepository flockRepository;

    @Autowired
    private FacultyRepository facultyRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView flock() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("flocks", flockRepository.findAll());
        map.put("faculties", facultyRepository.findAll());
        return new ModelAndView("flock", map);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Flock addFlock(@RequestBody FlockDTO dto) {
        if (dto.getName().isEmpty()) {
            return null;
        }
        Flock flock = flockRepository.findByName(dto.getName());
        if (flock != null) {
            throw new ExisistException();
        }
        return flockRepository.save(new Flock(dto.getName(), dto.getStartDate(), dto.getEndDate(), facultyRepository.findOne(Integer.valueOf(dto.getFaculty()))));
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteFlock(@RequestParam Integer id) {
        try {
            flockRepository.delete(id);
        } catch (Exception e) {
            throw new NotFoundException();
        }
        return flock();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public void deletePostFlock(@RequestParam Integer id) {
        try {
            flockRepository.delete(id);
        } catch (Exception e) {
            throw new NotFoundException();
        }
    }

}
