package com.kknau.dto;

public class AuditoryDTO {
    private String number;
    private String amountWorkPlace;
    private String auditoryType;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAmountWorkPlace() {
        return amountWorkPlace;
    }

    public void setAmountWorkPlace(String amountWorkPlace) {
        this.amountWorkPlace = amountWorkPlace;
    }

    public String getAuditoryType() {
        return auditoryType;
    }

    public void setAuditoryType(String auditoryType) {
        this.auditoryType = auditoryType;
    }

    @Override
    public String toString() {
        return "AuditoryDTO{" +
                "number='" + number + '\'' +
                ", amountWorkPlace=" + amountWorkPlace +
                ", auditoryType=" + auditoryType +
                '}';
    }
}
