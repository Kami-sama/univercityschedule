package com.kknau.dto;

public class FacultyDTO {
    private String name;
    private String abbreviate;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviate() {
        return abbreviate;
    }

    public void setAbbreviate(String abbreviate) {
        this.abbreviate = abbreviate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "FacultyDTO{" +
                "name='" + name + '\'' +
                ", abbreviate='" + abbreviate + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
