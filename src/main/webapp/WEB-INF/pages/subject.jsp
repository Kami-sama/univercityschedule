<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#subject_menu").addClass("active");
        $("#add_subject_button").click(function () {
            $.ajax({
                url: '../subject/add',
                data: $("#subject_name").val(),
                dataType: 'json',
                type: 'POST',
                contentType: 'text/html;charset=UTF-8',
                mimeType: 'application/json',
                success: function (subject) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#subject_table").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td>' + subject.name + '</td>' +
                            '<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteSubject(' + subject.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +
                            '</tr>');
                },
                statusCode: {
                    302: function () {
                        alert("Така дисципліна вже існує!");
                    }
                }
            });
        });
    });

    function deleteSubject(i) {
        $.ajax({
            url: '../subject/delete',
            data: "id=" + i,
            dataType: 'json',
            type: 'POST',
            statusCode: {
                200: function () {
                    location.reload(true);
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Дисципліни
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Додати ддисципліну</h3>
                        </div>
                        <div class="panel-body">
                            <label for="subject_name">Назва дисципліни:</label><input id="subject_name"
                                                                                       class="form-control">
                            <input type="button" value="Додати" id="add_subject_button"
                                   style="float: right; margin-top: 2%;" class="btn btn-success">
                        </div>
                    </div>
            </div>

            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="subject_table">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Назва</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="count" value="0" scope="page"/>
                        <c:forEach items="${subjects}" var="subject">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr>
                                <td>${count}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/subject/get?id=${subject.id}">${subject.name}</a>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                       onclick="deleteSubject(${subject.id});">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>

<jsp:include page="templates/footer.jsp"/>
