<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#teacher_menu").addClass("active");
        $("#add_teacher_button").click(function () {
            var param = {
                "name": $("#teacher_name").val(),
                "position": $("#teacher_position").val()
            };

            $.ajax({
                url: '../teacher/add',
                data: JSON.stringify(param),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (teacher) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#faculty_table").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td><a href="../teacher/get?id=' + teacher.id + '">' + teacher.name + '</a></td>' +
                            '<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteTeacher(' + teacher.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +
                            '</tr>');
                },
                statusCode: {
                    302: function () {
                        alert("Такий викладач вже існує!");
                    }
                }
            });
        });
    });

    function deleteTeacher(i) {
        $.ajax({
            url: '../teacher/delete',
            data: "id=" + i,
            dataType: 'json',
            type: 'POST',
            success: function () {
                location.reload(true);
            },
            statusCode: {
                200: function () {
                    location.reload(true);
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Викладачі
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <form role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Додати викладача</h3>
                        </div>
                        <div class="panel-body">
                            <label for="teacher_name">ПІБ викладача:</label><input id="teacher_name"
                                                                                       class="form-control">
                            <label for="teacher_position">Посада викладача:</label><input id="teacher_position"
                                                                                               class="form-control">
                            <input type="button" value="Додати" id="add_teacher_button"
                                   style="float: right; margin-top: 2%;" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="faculty_table">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>ПІБ</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="count" value="0" scope="page"/>
                        <c:forEach items="${teachers}" var="teacher">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr>
                                <td>${count}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/teacher/get?id=${teacher.id}">${teacher.name}</a>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                       onclick="deleteTeacher(${teacher.id});">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>

<jsp:include page="templates/footer.jsp"/>
