<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#auditory_menu").addClass("active");
        $("#add_laboratory_button").click(function () {
            var param = {
                "number": $("#number_auditory").val(),
                "amountWorkPlace": $("#amount_work_place").val(),
                "auditoryType": $("#auditory_type").val()
            };

            $.ajax({
                url: '../auditory/add',
                data: JSON.stringify(param),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (auditory) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#auditory_table").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td>' + auditory.number + '</td>' +
                            '<td>' + auditory.amountWorkPlace + '</td>' +
                            '<td>' + auditory.auditoryType + '</td>' +
                            '<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteAuditory(' + auditory.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +
                            '</tr>');
                },
                statusCode: {
                    302: function () {
                        alert("Така аудиторія вже існує!");
                    }
                }
            });
        });
    });

    function deleteAuditory(i) {
        $.ajax({
            url: '../auditory/delete',
            data: "id=" + i,
            dataType: 'json',
            type: 'POST',
            statusCode: {
                200: function () {
                    location.reload(true);
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Аудиторії
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <form role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Додати аудиторію</h3>
                        </div>
                        <div class="panel-body">
                            <label for="number_auditory">Номер аудиторії:</label><input id="number_auditory"
                                                                                        class="form-control">
                            <label for="amount_work_place">Кількість робочих місць:</label><input id="amount_work_place"
                                                                                                  type="number" min="0"
                                                                                                  class="form-control">
                            <label for="auditory_type">Тип аудиторії:</label>
                            <select class="form-control" id="auditory_type">
                                <c:forEach var="type" items="${lessonTypes}">
                                    <option value="${type}">${type.toString()}</option>
                                </c:forEach>
                            </select>
                            <input type="button" value="Додати" id="add_laboratory_button"
                                   style="float: right; margin-top: 2%;" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="auditory_table">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Номер</th>
                            <th>Клькість місць</th>
                            <th>Тип</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="count" value="0" scope="page"/>
                        <c:forEach items="${auditories}" var="auditory">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr>
                                <td>${count}</td>
                                <td>${auditory.number}</td>
                                <td>${auditory.amountWorkPlace}</td>
                                <td>${auditory.auditoryType.toString()}</td>
                                <td>
                                    <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                       onclick="deleteAuditory(${auditory.id});">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>

<jsp:include page="templates/footer.jsp"/>
