<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $("#subject_menu").addClass("active");
    function deleteTeacherFromSubject(i) {
        var param = "subject_id=" + ${subject.id} +"&teacher_id=" + i;
        $.ajax({
            url: '../teacher/subjectTeacherDelete',
            data: param,
            dataType: 'json',
            type: 'POST',
            success: function (status) {
                location.reload(true);
            },
            statusCode: {
                302: function () {
                    alert("Така комбінація вже існує!");
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${subject.name}
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Викладачі</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" id="subjects_teacher">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>ПІБ</th>
                                    <th>Посада</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="count" value="0" scope="page"/>
                                <c:forEach items="${teachers}" var="teacher">
                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                    <tr>
                                        <td>${count}</td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/teacher/get?id=${teacher.id}">${teacher.name}</a>
                                        </td>
                                        <td>${teacher.position}</td>
                                        <td>
                                            <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                               onclick="deleteTeacherFromSubject(${teacher.id});">
                                                <i class="fa fa-trash-o fa-lg"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>

    <jsp:include page="templates/footer.jsp"/>
