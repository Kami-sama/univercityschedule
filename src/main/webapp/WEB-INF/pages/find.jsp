<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#find_menu").addClass("active");

        $("#faculty").change(function () {
            getFlocksByFaculty($(this).val());
        });

        var day = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];

        $("#flock").change(function () {
            $.ajax({
                url: '../scheduler/getSchedulersByFlock',
                data: "id=" + $(this).val(),
                dataType: 'json',
                type: 'POST',
                success: function (schedulers) {
                    $("#scheduler_table_body").html('');
                    for (var i = 0; i < 11; i++) {
                        $("#scheduler_table_body").append('<tr id="row' + i + '"><td>' + i + '</td></tr>');
                        day.forEach(function (s) {
                            $("#row" + i).append('<td id="' + i + s + '"></td>');
                        });
                    }

                    if (schedulers.length != 0) {
                            for (var j = 0; j < 11; j++) {
                                schedulers.forEach(function (s) {
                                    var teacher = cutName(s.teacher.name);
                                    if (s.lessonNumber == j) {
                                        if (s.fraction == "DENOMINATOR") {
//                                           $("#" + s.lessonNumber + s.weekDay).append('<table class="table"><tr><td>' + s.auditory.number + '</td><td>' + s.subject.name + '</td><td>' + s.teacher.name + '</td></tr></table>');
                                            $("#" + s.lessonNumber + s.weekDay).append('<hr>' + s.auditory.number + '-' + s.subject.name + '-' + teacher);
//                                            $("#" + s.lessonNumber + s.weekDay).append('<td>' + s.subject.name + '</td>');
                                        } else {
                                            $("#" + s.lessonNumber + s.weekDay).append(s.auditory.number + '-' + s.subject.name + '-' + teacher);
//                                           $("#" + s.lessonNumber + s.weekDay).append('<table><tr><td>' + s.auditory.number + '</td><td>' + s.subject.name + '</td><td>' + s.teacher.name + '</td></tr></table>');
                                        }
                                    } else {
                                    }
                                });
                            }
                    }
                }
            });
        });
    });

    function cutName(s) {
        var t = [];
        t = s.split(' ');
        var l = t.length;
        if (l == 0) {
            return '';
        } else if (l <= 1) {
            return t[0];
        }
        return t[0] + ' ' + t[1].charAt(0) + '. ' + t[2].charAt(0) + '.';
    }

    function getFlocksByFaculty(idFaculty) {
        $.ajax({
            url: '../scheduler/getFlocksByFaculty',
            data: "id=" + idFaculty,
            dataType: 'json',
            type: 'POST',
            success: function (flocks) {
                $("#flock").html('<option value="">Оберіть групу</option>');
                flocks.forEach(function (flock) {
                    $("#flock").append('<option value="' + flock.id + '">' + flock.name + '</option>');
                });
            }
        });
    }
</script>
<div id="page-wrapper">

<div class="container-fluid">

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Пошук Розкладу
        </h1>
    </div>
</div>
<!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Розклад</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-3">
                        <select class="form-control" id="faculty">
                            <option value="">Оберіть факультет</option>
                            <c:forEach items="${faculties}" var="faculty">
                                <option value="${faculty.id}">${faculty.name}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control" id="flock">
                        </select>
                    </div>
                </div>
                <table class="table table-bordered table-hover table-striped" id="scheduler_table">
                    <thead>
                    <tr>
                        <td></td>
                        <c:forEach items="${days}" var="day">
                            <td>${day.name}</td>
                        </c:forEach>
                    </tr>
                    </thead>
                    <tbody id="scheduler_table_body">

                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

<jsp:include page="templates/footer.jsp"/>