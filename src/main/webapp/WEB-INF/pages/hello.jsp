<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/css/grayscale.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet"
          type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <i class="fa fa-play-circle"></i> <span class="light">KK NAU</span> Scheduler
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#download">Download</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="brand-heading">Курсовий проект</h1>

                    <p class="intro-text" style="text-shadow:
        -1px -1px 0 #000,
        1px -1px 0 #000,
        -1px 1px 0 #000,
         1px 1px 0 #000;
">Курсовий проект для розкладу занять у вузі.<br>Created by
                        Serhii Saviuk.</p>
                    <a href="/faculty" class="btn btn-default btn-lg"><span
                            class="network-name">Start DEMO</span></a><br/>
                    <a href="#about" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- About Section -->
<section id="about" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>О проекте</h2>

            <p>Розробити інформаційну підсистему РОЗКЛАД ЗАНЯТЬ У ВУЗІ (коледжі, технікумі). Розклад аудиторних занять у
                вузі , складене на семестр, включає відомості про назви предметів (навчальних дисциплін), видах
                аудиторних занять ( лекція, лабораторна робота , практичне заняття), прізвищах викладача (викладачів),
                позначеннях аудиторій, днями тижня і номерах «пар» (здвоєних академічних годин занять), показнику
                парності тижні проведення заняття («чисельник / знаменник»), ступеня « повноти » групи на занятті (група
                або підгрупи), позначеннях навчальних груп, номерах курсу, позначеннях інститутів (факультетів).</p>

            <p>У реальній ситуації потрібно також відстежувати відсутність « накладок » у розкладі ( «неперетинання»
                занять для викладачів, груп, аудиторій по парах і днях тижня, включаючи «чисельник / знаменник»,
                відсутність «вікон» у розкладі як для студентів, так і для викладачів, і т. д.) , що ускладнює
                завдання.</p>

            <p>Потрібен забезпечувати виведення на друк фрагментів розкладу занять ( для груп , курсів , інститутів (
                факультетів ) і т. д.). Розробити: меню програми і засоби діалогу, форми введення і зміни даних, пошук,
                фільтрація, сортування даних, запити, звіти для виведення на друк .</p>
        </div>
    </div>
</section>

<!-- Download Section -->
<section id="download" class="content-section text-center">
    <div class="download-section">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Скачать Исходники</h2>

                <p style="text-shadow:
        -1px -1px 0 #000,
        1px -1px 0 #000,
        -1px 1px 0 #000,
         1px 1px 0 #000;
">You can download Source Scheduler for free on the preview page at BitBucket.</p>
                <a href="https://bitbucket.org/Kami-sama/univercityschedule" class="btn btn-default btn-lg">Visit
                    BitBucket</a>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>Contact Serhii Saviuk</h2>

            <p>Feel free to email us to provide some feedback on our templates, give us suggestions for new templates
                and themes, or to just say hello!</p>

            <p><a href="mailto:sergei.savuk@gmail.com">sergei.savuk@gmail.com</a>
            </p>
            <ul class="list-inline banner-social-buttons">
                <li>
                    <a href="https://twitter.com/Kamisamas37" class="btn btn-default btn-lg"><i
                            class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                </li>
                <li>
                    <a href="https://github.com/Kami-sama" class="btn btn-default btn-lg"><i
                            class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                </li>
                <li>
                    <a href="https://www.facebook.com/kamisama.kami" class="btn btn-default btn-lg"><i
                            class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- Map Section -->
<div id="map"></div>

<!-- Footer -->
<footer>
    <div class="container text-center">
        <p>Copyright &copy; Kami-sama Incorporation 2014</p>
    </div>
</footer>

<!-- jQuery Version 1.11.0 -->
<script src="js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery.easing.min.js"></script>

<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

<!-- Custom Theme JavaScript -->
<script src="js/grayscale.js"></script>

</body>

</html>
