<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">

    var weekDay;
    var numberLesson;
    var fractionType;
    $(document).ready(function () {
        $("#scheduler_menu").addClass("active");
        $("#flock_faculty").change(function () {
            getFlocksByFaculty($(this).val());
        });

        $("#subjects").change(function () {
            getTeachersBySubject($(this).val());
        });

        $("#add_schedule_button").click(function () {
            var param = {
                "flock": $("#flocks").val(),
                "teacher": $("#teachers").val(),
                "auditory": $("#auditors").val(),
                "subject": $("#subjects").val(),
                "lessonNumber": numberLesson,
                "fraction": fractionType,
                "weekDay": weekDay
            };
            $.ajax({
                url: '../scheduler/add',
                data: JSON.stringify(param),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (msg) {
                    if (msg.status == 1) {
                        alert(msg.target + " зайняті у цей час");
                    } else {
                        alert("Пара додана у розклад");
                    }
                }
            });
        });

        getFlocksByFaculty($("#flock_faculty").val());
    });

    function getFlocksByFaculty(idFaculty) {
        $.ajax({
            url: '../scheduler/getFlocksByFaculty',
            data: "id=" + idFaculty,
            dataType: 'json',
            type: 'POST',
            success: function (flocks) {
                $("#flocks").html('');
                flocks.forEach(function (flock) {
                    $("#flocks").append('<option value="' + flock.id + '">' + flock.name + '</option>');
                });
            }
        });
    }

    function getTeachersBySubject(idSubject) {
        $.ajax({
            url: '../scheduler/getTeachersBySubject',
            data: "id=" + idSubject,
            dataType: 'json',
            type: 'POST',
            success: function (teachers) {
                $("#teachers").html('');
                teachers.forEach(function (teacher) {
                    $("#teachers").append('<option value="' + teacher.id + '">' + teacher.name + '</option>');
                });
            }
        });
    }

    function getAuditoriumsByLessonType(type) {
        $.ajax({
            url: '../scheduler/getAuditoriumsByLessonType',
            data: "type=" + type,
            dataType: 'json',
            type: 'POST',
            success: function (auditors) {
                $("#auditors").html('');
                auditors.forEach(function (auditor) {
                    $("#auditors").append('<option value="' + auditor.id + '">' + auditor.number + '<em> (' + auditor.amountWorkPlace + ' місць) </em></option>');
                });
            }
        });
    }

    function setWeekDay(s) {
        weekDay = s;
    }

    function setNumberLesson(i) {
        numberLesson = i;
    }

    function setFractionType(t) {
        fractionType = t;
    }

    function deleteScheduler(i) {
        $.ajax({
            url: '../scheduler/delete',
            data: "id=" + i,
            dataType: 'json',
            type: 'POST',
            statusCode: {
                200: function () {
                    $("#row-" + i).html("");
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Додати пару у розклад
                </h1>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Додати групу</h3>
            </div>
            <div class="panel-body">
                <div class="col-lg-6">
                    <div class="btn-group" id="weekDay" data-toggle="buttons">
                        <c:forEach items="${weekDay}" var="day">
                            <label class="btn btn-default">
                                <input type="radio" onchange="setWeekDay('${day}')">${day.name}
                            </label>
                        </c:forEach>
                    </div>
                    <label for="numberLesson">Оберіть номер пари:</label>

                    <div class="btn-group" id="numberLesson" data-toggle="buttons">
                        <c:forEach begin="0" end="10" step="1" var="i">
                            <label class="btn btn-default">
                                <input type="radio" onchange="setNumberLesson(${i})">${i}
                            </label>
                        </c:forEach>
                    </div>
                    <br/>
                    <br/>

                    <div class="btn-group" id="fractionType" data-toggle="buttons">
                        <c:forEach items="${fractionTypes}" var="type">
                            <label class="btn btn-default">
                                <input type="radio" onchange="setFractionType('${type}')">${type.toString()}
                            </label>
                        </c:forEach>
                    </div>

                    <br/>
                    <label for="flock_faculty">Факультет:</label>
                    <select id="flock_faculty" class="form-control">
                        <c:forEach var="faculty" items="${faculties}">
                            <option value="${faculty.id}">${faculty.name}</option>
                        </c:forEach>
                    </select>
                    <label for="flocks">Номер групи:</label>
                    <select id="flocks" class="form-control">
                    </select>


                </div>
                <div class="col-lg-6">

                    <div class="btn-group" id="lessonType" data-toggle="buttons">
                        <c:forEach items="${lessonTypes}" var="type">
                            <label class="btn btn-default">
                                <input type="radio" onchange="getAuditoriumsByLessonType('${type}')">${type.toString()}
                            </label>
                        </c:forEach>
                    </div>

                    <br/>
                    <label for="auditors">Аудіторія:</label>
                    <select id="auditors" class="form-control">
                    </select>

                    <label for="subjects">Дисципліна:</label>
                    <select id="subjects" class="form-control">
                        <option value="">Оберіть дисципліну</option>

                        <c:forEach items="${subjects}" var="subject">
                            <option value="${subject.id}">${subject.name}</option>
                        </c:forEach>
                    </select>
                    <label for="teachers">Викладач:</label>
                    <select id="teachers" class="form-control">
                    </select>

                    <input type="button" value="Додати" id="add_schedule_button"
                           style="float: right; margin-top: 2%;" class="btn btn-success">
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Група</th>
                            <th>Дисципліна</th>
                            <th>Аудиторія</th>
                            <th>Викладач</th>
                            <th>Пара</th>
                            <th>День Тижня</th>
                            <th>Фракція</th>
                            <th></th>
                        </tr>
                        </thead>
                        <c:set var="count" value="0" scope="page"/>
                        <c:forEach items="${schedulers}" var="scheduler">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr id="row-${scheduler.id}">
                                <td>${count}</td>
                                <td>${scheduler.flock.name}</td>
                                <td>${scheduler.subject.name}</td>
                                <td>${scheduler.auditory.number}</td>
                                <td>${scheduler.teacher.getCutName()}</td>
                                <td>${scheduler.lessonNumber}</td>
                                <td>${scheduler.weekDay.name}</td>
                                <td>${scheduler.fraction.toString()}</td>
                                <td>
                                    <a class="btn btn-sm btn-danger" href="#" id="deleteScheduler"
                                       onclick="deleteScheduler(${scheduler.id});">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- /#page-wrapper -->

<jsp:include page="templates/footer.jsp"/>
