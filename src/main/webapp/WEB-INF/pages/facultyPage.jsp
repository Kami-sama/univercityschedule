<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#faculty_menu").addClass("active");
        $("#add_flock_button").click(function () {
            var param = {
                "name": $("#flock_name").val(),
                "startDate": $("#flock_start_date").val(),
                "endDate": $("#flock_end_date").val(),
                "faculty": ${faculty.id}
            };

            $.ajax({
                url: '../flock/add',
                data: JSON.stringify(param),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (flock) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#flock_table").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td>' + flock.name + '</td>' +
                            '<td>' + flock.startDate + '</td>' +
                            '<td>' + flock.endDate + '</td>' +
                            '<td><a href="../faculty/delete?id=' + flock.id + '">Удалить</a></td>' +
                            '</tr>');
                    location.reload(true);
                }
            });
        });
    });
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Факультет "${faculty.name}"
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Інформація факультету</h3>
                    </div>
                    <div class="panel-body">
                        <label for="faculty_name">Назва факультету:</label>
                        <output id="faculty_name">${faculty.name}</output>
                        <label for="faculty_abbreviate">Абривіатура факультету:</label>
                        <output id="faculty_abbreviate">${faculty.abbreviate}</output>
                        <label for="faculty_phone">Телефон факультету:</label>
                        <output id="faculty_phone">${faculty.phone}</output>
                    </div>
                </div>
                <form role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Додати групу</h3>
                        </div>
                        <div class="panel-body">
                            <label for="flock_name">Номер групи:</label><input id="flock_name" class="form-control">
                            <label for="flock_start_date">Початок навчання:</label><input type="date"
                                                                                         id="flock_start_date"
                                                                                         class="form-control">
                            <label for="flock_end_date">Кінець навчання:</label><input type="date" id="flock_end_date"
                                                                                      class="form-control">
                            <input type="button" value="Додати" id="add_flock_button"
                                   style="float: right; margin-top: 2%;" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Групи що належать факультету</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" id="flock_table">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Номер групи</th>
                                    <th>Початок навчання</th>
                                    <th>Кінець навчання</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="count" value="0" scope="page"/>
                                <c:forEach items="${flocks}" var="flock">
                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                    <tr>
                                        <td>${count}</td>
                                        <td>${flock.name}</td>
                                        <td>${flock.startDate}</td>
                                        <td>${flock.endDate}</td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/flock/delete?id=${flock.id}">Видалити</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>

    <jsp:include page="templates/footer.jsp"/>
