<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#faculty_menu").addClass("active");
        $("#add_faculty_button").click(function () {
            var param = {
                "name": $("#faculty_name").val(),
                "abbreviate": $("#faculty_abbreviate").val(),
                "phone": $("#faculty_phone").val()
            };

            $.ajax({
                url: '../faculty/add',
                data: JSON.stringify(param),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (faculty) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#faculty_table").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td><a href="../faculty/get?id=' + faculty.id + '">' + faculty.abbreviate + '</a></td>' +
                            '<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteFaculty(' + faculty.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +
                            '</tr>');
                },
                statusCode: {
                    302: function () {
                        alert("Такий факультет вже існує!");
                    }
                }
            });
        });
    });

    function deleteFaculty(i) {
        $.ajax({
            url: '../faculty/delete',
            data: "id=" + i,
            dataType: 'json',
            type: 'POST',
            statusCode: {
                200: function () {
                    location.reload(true);
                }
            }
        });
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Факультети
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <form role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Додати факультет</h3>
                        </div>
                        <div class="panel-body">
                            <label for="faculty_name">Назва Факультету:</label><input id="faculty_name"
                                                                                      class="form-control">
                            <label for="faculty_abbreviate">Абривіатура факультету:</label><input
                                id="faculty_abbreviate" class="form-control">
                            <label for="faculty_phone">Телефон факультету:</label><input id="faculty_phone"
                                                                                         class="form-control">
                            <input type="button" value="Додати" id="add_faculty_button"
                                   style="float: right; margin-top: 2%;" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="faculty_table">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Абривіатура</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="count" value="0" scope="page"/>
                        <c:forEach items="${faculties}" var="faculty">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr>
                                <td>${count}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/faculty/get?id=${faculty.id}">${faculty.abbreviate}</a>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                       onclick="deleteFaculty(${faculty.id});">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>

<jsp:include page="templates/footer.jsp"/>
