<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/headerMenu.jsp"/>
<script language="JavaScript">
    $(document).ready(function () {
        $("#teacher_menu").addClass("active");
        $("#add_subject_button").click(function () {
            var param = $("#subjects").val() + ",${teacher.id}";

            $.ajax({
                url: '../teacher/addSubject',
                data: param,
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                mimeType: 'application/json',
                success: function (subject) {
                    var i = parseInt($("tr:last-child td:first-child").text()) + 1;
                    if (isNaN(i)) {
                        i = 1;
                    }
                    $("#subjects_teacher").append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td>' + subject.name + '</td>' +
                            '<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteSubject(' + subject.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +
                            '</tr>');
                    location.reload(true);
                },
                statusCode: {
                    302: function () {
                        alert("Така комбінація вже існує!");
                    }
                }
            });
        });
    });

    function deleteSubject(x) {
        var param = "subject_id=" + x + "&teacher_id=${teacher.id}";
        $.ajax({
            url: '../teacher/subjectTeacherDelete',
            data: param,
            dataType: 'json',
            type: 'POST',
            success: function (status) {
                location.reload(true);
            }
        });

        <%--$.ajax({--%>
        <%--url: '../teacher/getSubjectByTeacher',--%>
        <%--data: ${teacher.id},--%>
        <%--dataType: 'json',--%>
        <%--type: 'POST',--%>
        <%--contentType: 'application/json',--%>
        <%--mimeType: 'application/json',--%>
        <%--success: function (subjects) {--%>
        <%--subjects.forEach(function (subject) {--%>
        <%--$("#tbody_subject").html("");--%>
        <%--$("#subjects_teacher").append('<tr>' +--%>
        <%--'<td>' + i + '</td>' +--%>
        <%--'<td>' + subject.name + '</td>' +--%>
        <%--'<td><a class="btn btn-sm btn-danger" href="#" id="deleteSubject" onclick="deleteSubject(' + subject.id + ');"><i class="fa fa-trash-o fa-lg"></i></a></td>' +--%>
        <%--'</tr>');--%>
        <%--});--%>
        <%--}--%>
        <%--});--%>
    }
</script>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${teacher.name}
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Інформація про викладача</h3>
                    </div>
                    <div class="panel-body">
                        <label for="faculty_name">ПІБ викладача:</label>
                        <output id="faculty_name">${teacher.name}</output>
                        <label for="faculty_abbreviate">Посада викладача:</label>
                        <output id="faculty_abbreviate">${teacher.position}</output>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Додати дисципліну</h3>
                    </div>
                    <div class="panel-body">
                        <label for="subjects">Дисципліни:</label>
                        <select id="subjects">
                            <c:forEach items="${subjects}" var="subject">
                                <option value="${subject.id}">${subject.name}</option>
                            </c:forEach>
                        </select>

                        <input type="button" value="Додати" id="add_subject_button"
                               style="float: right; margin-top: 2%;" class="btn btn-success">
                    </div>
                </div>

            </div>

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Дисципліни які викладає</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" id="subjects_teacher">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Назва дисципліни</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="tbody_subject">
                                <c:set var="count" value="0" scope="page"/>
                                <c:forEach items="${teacher.subjects}" var="subject">
                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                    <tr>
                                        <td>${count}</td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/subject/get?id=${subject.id}">${subject.name}</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-danger" href="#" id="deleteSubject"
                                               onclick="deleteSubject(${subject.id});">
                                                <i class="fa fa-trash-o fa-lg"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>

    <jsp:include page="templates/footer.jsp"/>
